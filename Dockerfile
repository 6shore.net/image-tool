FROM debian

LABEL maintainer="Tomáš Kukrál"

RUN apt-get update && \
  apt-get install -y --no-install-recommends \
  ca-certificates \
  curl \
  dnsutils \
  gettext-base \
  iperf \
  iproute2 \
  jq \
  ncdu \
  net-tools \
  nmap \
  nnn \
  openssh-client \
  openssl \
  python3 \
  rsync \
  strace \
  systemd \
  tcpdump \
  traceroute \
  tmux \
  tree \
  vim-tiny \
  wget \
  && rm -rf /var/lib/apt/lists/*

# link vim.tiny to vim
RUN ln -s /usr/bin/vim.tiny /usr/bin/vim

# install certigo
RUN curl -Lo /usr/local/bin/certigo https://github.com/square/certigo/releases/download/v1.11.0/certigo-linux-amd64 && chmod +x /usr/local/bin/certigo

# install croc
RUN curl -L https://github.com/schollz/croc/releases/download/v8.3.2/croc_8.3.2_Linux-64bit.tar.gz | tar -C /usr/local/bin -xvz croc && chmod +x /usr/local/bin/croc

# install kubectl
RUN curl -Lo /usr/local/bin/kubectl https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl && chmod +x /usr/local/bin/kubectl

# install crictl
RUN curl -L https://github.com/kubernetes-sigs/cri-tools/releases/download/v1.31.1/crictl-v1.31.1-linux-amd64.tar.gz | tar -C /usr/local/bin -xvz crictl && chmod +x /usr/local/bin/crictl

